import download from './download.png';
import logo from './banner.png';
import preview from './preview.png';

export {
  download,
  logo,
  preview,
};
