import React, { useState, useEffect } from "react";
import { Card, FormField, Loader } from "../components";
import { Link } from "react-router-dom";
import { useCallback } from "react";
const RenderCards = ({ data, title }) => {
  if (data?.length > 0) {
    return data.map((post) => <Card key={post._id} {...post} />);
  }
  return (
    <h2
      className="mt-5 font-bold text-[#666e75] 
    text-xl uppercase"
    >
      {title}
    </h2>
  );
};

const home = () => {
  const [loading, setLoading] = useState(false);
  const [allPosts, setAllPosts] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [searchResults, setSearchResults] = useState(null);
  const [searchTimeout, setSearchTimeout] = useState(null);
  const fetchPosts = useCallback(async () => {
    setLoading(true);
    const controller = new AbortController();

    try {
      const response = await fetch(
        `https://artrigenius-backend.onrender.com/api/v1/post`,
        { signal: controller.signal }
      );

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const { data } = await response.json();
      setAllPosts(data.reverse());
    } catch (err) {
      alert(err);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchPosts();

    return () => {
      const controller = new AbortController();
      controller.abort();
    };
  }, [fetchPosts]);

  const handleSearchChange = (e) => {
    setSearchText(e.target.value);
    setSearchTimeout(
      setTimeout(() => {
        const searchResults = allPosts.filter(
          (item) =>
            item.name.toLowerCase().includes(searchText.toLowerCase()) ||
            item.prompt.toLowerCase().includes(searchText.toLowerCase())
        );
        setSearchResults(searchResults);
      }, 500)
    );
  };

  return (
    <section className="max-w-7xl mx-auto">
      <div>
        <Link
          to="/create-post"
          className="relative m-5 no-underline flex justify-center 
          items-center cursor-pointer font-poppins py-3 bg-slate-200
          text-black font-light text-lg z-0 overflow-hidden rounded-md
          hover:bg-[#6469ff] hover:text-white px-1"
        >
          Want to use the ImageAI?
        </Link>

        <h1 className="font-extrabold text-[#222328] text-[32px]">
          The community showcase
        </h1>
        <p className="mt-2 text-[#66e75] text-[14px] max-w-[500px]">
          Browse through a collection of user generated images
        </p>
      </div>
      <div className="mt-16">
        <FormField
          labelName="Search posts"
          type="text"
          name="text"
          placeholder="What do you wanna see?"
          value={searchText}
          handleChange={handleSearchChange}
        />
      </div>
      <div className="mt-10">
        {loading ? (
          <div className="flex justify-center items-center">
            <Loader />
          </div>
        ) : (
          <>
            {searchText && (
              <h2 className="font-medium text-[#666e75] text-xl mb-3">
                showing results for{" "}
                <span className="x-#222328">{searchText}</span>
              </h2>
            )}
            <div
              className="grid lg:grid-cols-4 
            sm:grid-cols-3 xs:grid-cols-2 grid-cols-1 gap-3"
            >
              {searchText ? (
                <RenderCards
                  data={searchResults}
                  title="No search results found"
                />
              ) : (
                <RenderCards data={allPosts} title="no posts found" />
              )}
            </div>
          </>
        )}
      </div>
    </section>
  );
};

export default home;
